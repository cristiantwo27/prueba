package com.example.funciones

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //area_rectangulo(15.0, 10.0)
        area_rectangulo()
        Log.i("AREA RECTANGULO", area_rectangulo().toString())
        area_circulo()
        Log.i("AREA CIRCULO", area_circulo().toString())
        relacion()
        // Log.i("RELACION",relacion().toString())
    }


    fun area_rectangulo(): Double {
        var area = 0.0
        var base = 15.0
        var altura = 10.0
        area = base * altura
        return area
    }

    fun area_circulo(): Double {
        var area = 0.0
        val pi = 3.1416
        val radio = 5.0

        area = pi * (radio * radio)
        return area
    }

    fun relacion() {

        Log.i("prueba","eaaaaa")
        var num1 = 5
        var num2 = 10
        if (num1 > num2) {
            println("1")
        } else if (num1 == num2) {
            println("0")
        } else {
            println("-1")
        }
    }

}